# Opdracht Front-end Ontwikkelaar

## Doel van de opdracht

Ontwikkel een statische HTML-pagina gebaseerd op het aangeleverde design in de bijlage. De focus ligt op het nauwkeurig realiseren van het design en het zorgen voor een responsive website. De opdracht omvat de implementatie van verschillende componenten, zoals de header, hero, assortiment grid, producten slider, review slider, tekst media blok, blog slider en footer. Een functionele website is niet verplicht, maar als je ervoor kiest om functionaliteit toe te voegen dan wordt dit wel gewaardeerd. Je hebt de vrijheid om de pagina statisch op te bouwen of gebruik te maken van Wordpress. Let op een duidelijke structuur van je code.

## Ontwerpbestanden en Hulpmiddelen

- **Design Bestand**: Het ontwerp is beschikbaar in een Sketch bestand en in Figma.
  - **Windows Gebruikers**: Open met Lunacy of Adobe XD
  - **Mac Gebruikers**: Open met Sketch
  - **Figma url**: https://www.figma.com/file/VN6B8lNReTN4vEMpIX9msp/Testshop?type=design&node-id=0%3A2&mode=design&t=xB6U8OtriZKUBVj4-1
- **Bijlagen**:
  - **Afbeeldingen**: Afbeeldingen zijn beschikbaar in een aparte map.
  - **Fonts**:
    - **Font Awesome**: Gratis versie te downloaden vanaf de Font Awesome website.
    - **Poppins**: Deze kun je implementeren vanaf Google Fonts.

## Ontwerpcomponenten

### Header
- Logo
- Zoekbalk
- 3 Iconen

### Hero
- Afbeelding
- Tekst
- Knop

### Assortiment Grid
- 6 foto’s in een responsive grid

### Producten Slider
- Slider met producten (Swiper)
- Gedeeltelijke achtergrondkleur

### Review Slider
- Links: Totaalscore
- Rechts: Tekst reviews in een slider (Swiper)

### Tekst Media Blok
- Links: 2 afbeeldingen
- Rechts: Tekst en een knop

### Blog Slider
- Blog berichten in een slider (Swiper)

### Footer
- Bedrijfsinformatie
- Links
- Nieuwsbrief formulier
- Betaal iconen

## Ontwikkelingseisen

### Must-Have
- HTML5
- CSS/SCSS (SCSSS kun je eventueel compilen via Koala GUI of met Gulp/Grunt/Webpack)
- Vanilla JS
- Swiper Slider (https://swiperjs.com/)
- Responsive design op alle schermgroottes
- Hover effecten op knoppen en klikbare afbeeldingen

### Optioneel
- Animaties bij scrollen langs een blok
- Gebruik van een CSS framework zoals Bootstrap

## Extra informatie

- **Type pagina**: Statische pagina.
- **Interactiviteit**: De functionaliteit van knoppen, links en formulier zijn niet verplicht.
